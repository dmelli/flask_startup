from flask import Blueprint, jsonify, request
from app.models.models import User, db, user_schema, users_schema
user = Blueprint('user_blueprint', __name__,
                 url_prefix='/user')


@user.errorhandler(Exception)
def handle_error(e):
    code = 400
    return jsonify(error=str(e)), code


# endpoint to show all users
@user.route("/", methods=["GET"])
def get_user():
    all_users = User.query.all()
    result = users_schema.dump(all_users)
    return jsonify(result.data)


# endpoint to get user detail by id
@user.route("/<id>", methods=["GET"])
def user_detail(id):
    user = User.query.get(id)

    if user is None:
        return jsonify(error="User not found"), 404

    return user_schema.jsonify(user)


@user.route('/', methods=['POST'])
def add_user():
    email = request.json['email']
    passw = request.json['password']

    new_user = User(email, passw)

    db.session.add(new_user)
    db.session.commit()

    return user_schema.jsonify(new_user)


# endpoint to update user
@user.route("/<id>", methods=["PUT"])
def user_update(id):
    user = User.query.get(id)

    if user is None:
        return jsonify(error="User not found"), 404

    user.email = request.json['email']
    user.password = request.json['password']

    db.session.commit()
    return user_schema.jsonify(user)


# endpoint to delete user
@user.route("/<id>", methods=["DELETE"])
def user_delete(id):
    user = User.query.get(id)
    if user is None:
        return jsonify(error="User not found"), 404

    db.session.delete(user)
    db.session.commit()

    return jsonify()
