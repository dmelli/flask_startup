from sqlalchemy.inspection import inspect

def serialize(object, no_attribs=[]):
    # atributes =[at  inspect(object).attrs.keys() not in []
    # print(atributes)
    return {c: getattr(object, c) for c in inspect(object).attrs.keys() if not c in no_attribs}


def serialize_list(list, no_attribs=[]):
    return [serialize(m, no_attribs) for m in list]