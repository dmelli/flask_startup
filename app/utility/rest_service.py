import requests

def post(uri, data, headers):
    req = requests.post(uri, json=data, headers=headers)

    if(req.status_code not in [200,201]):
        raise ValueError(req.json())
        return

    return req.status_code

def get(uri, headers):
    req = requests.get(uri, headers=headers)

    if(req.status_code not in [200,201]):
        raise ValueError(req.json())
        return

    return req.status_code, req.json()