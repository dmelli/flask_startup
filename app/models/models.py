from flask_sqlalchemy import SQLAlchemy
from flask_marshmallow import Marshmallow

db = SQLAlchemy()
ma = Marshmallow()


class User(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    email = db.Column(db.String(120), unique=True)
    password = db.Column(db.String(120))

    def __init__(self, email, password):
        self.email = email
        self.password = password


class UserSchema(ma.Schema):
    class Meta:
        # Fields to expose
        fields = ['id', 'email']

class UsersSchema(ma.Schema):
    class Meta:
        # Fields to expose
        fields = ['id', 'email']


user_schema = UserSchema()
users_schema = UsersSchema(many=True)