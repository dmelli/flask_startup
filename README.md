#Install first time:
##Create virtual env
virtualenv -p python3 flask_startup/

##Activate environment
cd flask_startup/
source bin/activate

##install dependencies
pip install -r requirement.txt


#Run
python run.py


#Public
On public folder have postman

#Configurations
config.py have all configuration.
For default have sqlite temporal databse

