class Config(object):
    DEBUG = True
    SQLALCHEMY_DATABASE_URI = 'sqlite:////tmp/test.db'


class Development(Config):
    DEBUG = False
    SQLALCHEMY_DATABASE_URI = 'sqlite:////tmp/test.db'
    SQLALCHEMY_TRACK_MODIFICATIONS = False



class Production(Config):
    DEBUG = False
    SQLALCHEMY_DATABASE_URI = 'sqlite:////tmp/test.db'
