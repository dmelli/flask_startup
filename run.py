from __future__ import unicode_literals
import os
from flask import Flask, jsonify
from flask_cors import CORS
from app.user.controllers import user as user
from app.models.models import db

app = Flask(__name__)
cors = CORS(app)

# Configurations
try:
    env = os.environ['APPLICATION_ENV']
except KeyError as e:
    env = 'Development'
app.config.from_object('config.%s' % env)

@app.errorhandler(404)
def not_found(error):
    return jsonify(error=str(error)), 404

@app.errorhandler(Exception)
def handle_error(e):
    code = 500
    return jsonify(error=str(e)), code

def initialize_app(flask_app):
    flask_app.register_blueprint(user)

    db.init_app(flask_app)
    with app.app_context():
        db.engine.echo = True
        db.metadata.bind = db.engine
        #db.drop_all() #uncomment to drop database
        db.metadata.create_all(checkfirst=True)


if __name__ == "__main__":
    initialize_app(app)
    port = 7082
    app.path = os.path.abspath(os.path.dirname(__file__))
    app.run(host='0.0.0.0', port=port, threaded=True, debug=True)